import numpy as np
import cv2
import math
import sys
import os
import hough_transform as ht


class LineDetector:
    def __init__(self, img, max_edge_plx=0.5, peak=1):
        print(img.shape)
        self.img = img
        self.max_edge_plx = max_edge_plx
        self.peak = peak

    def binary_edge(self, img):
        dx, dy = np.gradient(img)
        mag = cv2.magnitude(dx, dy)
        mag_norm = cv2.normalize(mag, None, 0, 1, cv2.NORM_MINMAX)
        ret = np.zeros(img.shape, np.uint8)
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                ret[i][j] = 1 if mag_norm[i][j]>0.1 else 0
        self.edge_img = ret
        return ret

    def hough_transform(self, img, edge_img, bin_size, max_edge_plx, peak):
        bin_x = max(img.shape[1]//100, bin_size)
        bin_y = max(img.shape[0]//100, bin_size * img.shape[0]//img.shape[1])
        print(bin_x)
        print(bin_y)
        print(img.shape[0]//bin_y)
        print(img.shape[1]//bin_x)
        self.bin_x = bin_x
        self.bin_y = bin_y
        self.max_edge_plx = max_edge_plx
        self.peak = peak

        ret = np.zeros((img.shape[0], img.shape[1], 3), np.uint8)
        distMax = np.sqrt(bin_x**2 + bin_y**2).astype(int)
        theta_rng = np.arange(0, 180, 1)
        rho_rng = np.arange(-distMax, distMax, 1)
        # print("distmax = " + str(distMax))
        # votes = np.zeros((len(rho_rng), len(theta_rng)), int)
        ds = []
        thetas = []
        x_idx = []
        y_idx = []
        votes_ids = []
        flag = 0
        tmp_flag = 0
        votes_cpy = np.zeros((len(rho_rng), len(theta_rng)), np.int8)
        for y in range(img.shape[0]//bin_y):
            for x in range(img.shape[1]//bin_x):
                votes = np.zeros((len(rho_rng), len(theta_rng)), np.uint8)
                for iy in range(bin_y):
                    for ix in range(bin_x):
                        if edge_img[y*bin_y+iy][x*bin_x+ix]:
                            flag += 1
                            for iTheta in range(len(theta_rng)):
                                t = np.deg2rad(theta_rng[iTheta])
                                dist = (ix * np.cos(t) + iy*np.sin(t)).astype(int)
                                iDist = dist + distMax
                                votes[iDist][iTheta] += 1
                if 2 <= flag < self.max_edge_plx*bin_x*bin_y:
                    if flag > tmp_flag:
                        votes_cpy = votes.copy()
                        tmp_flag = flag
                    tmp_ds = []
                    tmp_thetas = []
                    for i in range(self.peak):
                        idx = list(zip(*np.where(votes==np.max(votes))))[0]
                        tmp_ds.append(idx[0])
                        tmp_thetas.append(idx[1])
                        votes[idx[0]][idx[1]] = 0
                    tar_ds = np.average(tmp_ds).astype(int)
                    tar_thetas = np.average(tmp_thetas).astype(int)
                    ds.append(tar_ds)
                    thetas.append(tar_thetas)
                    x_idx.append(x)
                    y_idx.append(y)
                    flag = 0
                    # votes_cpy = votes.copy()
        cv2.normalize(votes_cpy, None, 0, 255, cv2.NORM_L1)
        cv2.imshow("VOTES", votes_cpy)
        ds = np.array(ds) - distMax
        thetas = np.array(thetas)
        self.ds = ds
        self.thetas = thetas
        self.x_idx = x_idx
        self.y_idx = y_idx
        print(len(ds))
        # print(thetas)
        # print(max(x_idx))
        # print(max(y_idx))
        for i in range(len(ds)):
            # Draw line
            ri = np.deg2rad(self.thetas[i])
            a = np.cos(ri)
            b = np.sin(ri)
            c = -self.ds[i]
            color = (0,255,0)
            # print("a = " + str(a) + "b = " + str(b) + "c = " + str(c))
            self.draw_line(ret, a, b, c, x_idx[i], y_idx[i], color, 0)
        # self.lse_refine(ret, ds, thetas, bin_x, bin_y, x_idx, y_idx)
        return ret

    def lse_refine(self, ret):
        tmp_ret = ret.copy()
        ret = np.dstack((ret, ret))
        ret = np.dstack((ret, tmp_ret))
        refined = np.zeros(ret.shape, np.uint8)
        ds = self.ds
        thetas = self.thetas
        bin_x = self.bin_x
        bin_y = self.bin_y
        x_idx = self.x_idx
        y_idx = self.y_idx
        color = (0,0,255)

        yellow = (0,255,255)
        blue = (255,0,0)
        flag = 0
        a = 1
        b = 1
        c = 1
        for i in range(len(ds)):
            D_mat = []
            ys = []
            A_mat = []
            for y in range(bin_y):
                for x in range(bin_x):
                    if self.edge_img[y+y_idx[i]*bin_y][x+x_idx[i]*bin_x]:
                        flag += 1
                        D_mat.append([x+x_idx[i]*bin_x, y+y_idx[i]*bin_y, 1])
                        A_mat.append([x+x_idx[i]*bin_x, 1])
                        ys.append(y+y_idx[i]*bin_y)
                        if i%2:
                            ret[y+y_idx[i]*bin_y][x+x_idx[i]*bin_x] = yellow
                        else:
                            ret[y+y_idx[i]*bin_y][x+x_idx[i]*bin_x] = blue
            if 2 <= flag < self.max_edge_plx*bin_x*bin_y:
            # if flag >= 5:
                D = np.array(D_mat).reshape((-1, 3))
                # print(D)
                A = np.array(A_mat).reshape((-1, 2))
                S = np.dot(np.transpose(D), D)

                try:
                    mb = np.dot(np.linalg.inv(np.dot(np.transpose(A),A)),
                        np.dot(np.transpose(A), ys))
                    a = -mb[0]
                    b = 1
                    c = -mb[1]
                except:
                    eval, evec = np.linalg.eig(S)
                    # print(eval)
                    # print(evec)
                    tmp = 255
                    for e in range(len(eval)):
                        if eval[e] < tmp:
                            a = evec[e][0]
                            b = -evec[e][1]
                            c = evec[e][2]
                            tmp = eval[e]
                # a = -mb[0]
                # b = 1
                # c = -mb[1]
                flag = 0
            self.draw_line(refined, a, b, c, x_idx[i], y_idx[i], color, 1)
            self.draw_line(ret, a, b, c, x_idx[i], y_idx[i], color, 1)
        return ret, refined

    def draw_line(self, ret, a, b, c, idx, idy, color, ref):
        if ref == 0:
            points = []
            xmin = 0
            xmax = self.bin_x
            ymin = 0
            ymax = self.bin_y

            if ymin <= (-c-a*xmin)/(b) < ymax:
                points.append((xmin+idx*self.bin_x,
                           ((-c-a*xmin)/(b)).astype(int)+idy*self.bin_y))
            if ymin <= (-c-a*xmax)/(b) < ymax:
                points.append((xmax+idx*self.bin_x,
                           ((-c-a*xmax)/(b)).astype(int)+idy*self.bin_y))
            if xmin <= (-c-b*ymin)/(a) < xmax:
                points.append((((-c-b*ymin)/(a)).astype(int)+idx*self.bin_x,
                           ymin+idy*self.bin_y))
            if xmin <= (-c-b*ymax)/(a) < xmax:
                points.append((((-c-b*ymax)/(a)).astype(int)+idx*self.bin_x,
                           ymax+idy*self.bin_y))
            if len(points) >=2:
                # print("success")
                cv2.line(ret, points[0], points[1], color, 1)
            # else:
                # print("ERR a="+str(a) + "b="+str(b)+"c="+str(c) + "("+ str(idx) +
                #   ", " +str(idy)+")")

        else:
            points = []
            xmin = self.bin_x*idx
            xmax = min(self.bin_x*(idx+1), self.edge_img.shape[1])
            ymin = self.bin_y*idy
            ymax = min(self.bin_y*(idy+1), self.edge_img.shape[0])

            if ymin <= (-c-a*xmin)/(b) < ymax:
                points.append((xmin,
                           ((-c-a*xmin)/(b)).astype(int)))
            if ymin <= (-c-a*xmax)/(b) < ymax:
                points.append((xmax,
                           ((-c-a*xmax)/(b)).astype(int)))
            if xmin <= (-c-b*ymin)/(a) < xmax:
                points.append((((-c-b*ymin)/(a)).astype(int),
                           ymin))
            if xmin <= (-c-b*ymax)/(a) < xmax:
                points.append((((-c-b*ymax)/(a)).astype(int),
                           ymax))
            if len(points) >=2:
                # print("success")
                cv2.line(ret, points[0], points[1], color, 1)
            # else:
                # print("ERR a="+str(a) + "b="+str(b)+"c="+str(c) + "("+ str(idx) +
                #   ", " +str(idy)+")")
        # cv2.imshow("LINE", ret)


if __name__ == '__main__':
    a_r = 8
    b_r = 0.3
    c_r = 1
    a_t = 1
    b_t = 1
    c_t = 1
    o_t = 1
    e_img = 0
    h_img = 0
    r_img = 0
    while(True):

        k = cv2.waitKeyEx(100) & 0xEFFFFF
        if len(sys.argv) >= 2 and os.path.isfile(sys.argv[1]):
            file_path = sys.argv[1]
        else:
            cv2.namedWindow("Camera")
            cap = cv2.VideoCapture(0)
            rect, cam = cap.read()
            cv2.imshow("Camera", cam)
            if k == ord(' '):
                file_path = '../data/save.jpg'
                cv2.imwrite(file_path)
                cap.release()
                cv2.destroyWindow("Camera")
                continue

        img = cv2.imread(file_path, 0)
        cv2.imshow("Image", img)
        if k == ord('q'):
            cv2.destroyAllWindows()
            break
        elif k == ord('e'):
            LD = LineDetector(img)
            cv2.imshow("Binary", LD.binary_edge(img)*255)
            continue
        elif k == ord('a'):
            LD = LineDetector(img)
            edge_img = LD.binary_edge(img)*255
            cv2.imshow("Binery Edge Image", edge_img)
            hough_img = LD.hough_transform(img, edge_img, a_r, b_r, c_r)
            refine_img, refine_line = LD.lse_refine(edge_img)
            cv2.imshow("Hough", hough_img)
            cv2.imshow("Refine", refine_img)
            cv2.imshow("Refine Line", refine_line)
            e_img = edge_img.copy()
            h_img = hough_img.copy()
            r_img = refine_line.copy()
            a_t += 1
            a_r = 2**(a_t%5+4)
            if a_r > 128:
                a_r = 8
            continue
        elif k == ord('b'):
            LD = LineDetector(img)
            edge_img = LD.binary_edge(img)*255
            cv2.imshow("Binery Edge Image", edge_img)
            hough_img = LD.hough_transform(img, edge_img, a_r, b_r, c_r)
            refine_img, refine_line = LD.lse_refine(edge_img)
            cv2.imshow("Hough", hough_img)
            cv2.imshow("Refine", refine_img)
            cv2.imshow("Refine Line", refine_line)
            e_img = edge_img.copy()
            h_img = hough_img.copy()
            r_img = refine_line.copy()
            b_t += 1
            b_r = 0.3+(b_t%5+1)/10
            if b_r > 0.8:
                b_r = 0.3
            continue
        elif k == ord('c'):
            LD = LineDetector(img)
            edge_img = LD.binary_edge(img)*255
            cv2.imshow("Binery Edge Image", edge_img)
            hough_img = LD.hough_transform(img, edge_img, a_r, b_r, c_r)
            refine_img, refine_line = LD.lse_refine(edge_img)
            cv2.imshow("Hough", hough_img)
            cv2.imshow("Refine", refine_img)
            cv2.imshow("Refine Line", refine_line)
            e_img = edge_img.copy()
            h_img = hough_img.copy()
            r_img = refine_line.copy()
            c_t += 1
            c_r = 1 + c_t%5
            if c_r > 5:
                c_r = 1
            continue
        elif k == ord('o'):
            LD = LineDetector(img)
            edge_img = LD.binary_edge(img)*255
            cv2.imshow("Binery Edge Image", edge_img)
            hough_img = LD.hough_transform(img, edge_img, a_r, b_r, c_r)
            refine_img, refine_line = LD.lse_refine(edge_img)
            cv2.imshow("Hough", hough_img)
            cv2.imshow("Refine", refine_img)
            cv2.imshow("Refine Line", refine_line)
            e_img = edge_img.copy()
            e_tmp = e_img.copy()
            e_img = np.dstack((e_img, e_img))
            e_img = np.dstack((e_img, e_tmp))
            h_img = hough_img.copy()
            r_img = refine_line.copy()
            overlay = cv2.addWeighted(e_img, 0.5, h_img, 0.2, 0)
            o_t += 1
            if o_t%2:
                overlay = cv2.addWeighted(overlay, 0.5, r_img, 0.2, 0)
            cv2.imshow("Over Lay", overlay)
            continue
        elif k == ord('h'):
            print("This program uses Hough transform to detect lines in image.")
            print("This program takes one argument which is the path to image")
            print("If no argument is given, this program will open the camera and take a picture when the user press space key, and save it as 'save.jpg' in ../data/ folder")
            print("Interactive Keys: ")
            print("e: Show binary line detection of image")
            print("a: Show line detection using Hough transform with different bin size")
            print("b: Show line detection using Hough transform with different max number of edge pixels")
            print("c: line detection using Hough transform with different peak threashold")
            print("o: Display the overlay before and after the lse refine")
            print("h: Display instructions")
            print("q: quit")
            continue
