## TO RUN:
```sh
$ python3 img_loader.py <image path that you want to open>
```
If you want to use the camera to capture the image, run:
```sh	
$ python3 img_loader.py
```
