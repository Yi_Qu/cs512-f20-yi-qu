import numpy as np
cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)


def hough_transform(unsigned char[:,:] edge_img,
                     double[:,:] theta,
                     double[:,:] d,
                     double[:,:] angs,
                     int cols, int rows,
                     int bin_x, int bin_y):
    for j from 0<=j<cols:
        for i from 0<=i<rows:
            if edge_img[j][i]:
                d[j][i] = i % bin_x * np.cos(angs[j][i]) + j % bin_y * np.sin(angs[j][i])
                theta[j][i] = angs[j][i]

