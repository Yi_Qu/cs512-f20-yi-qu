import numpy as np
import cv2
import tensorflow as tf
import sys

model = tf.keras.models.load_model('MNIST.h5')

while(True):
    k = cv2.waitKey(100) & 0xEFFFF
    if len(sys.argv) >= 2:
        file_path = sys.argv[1]
        img = cv2.imread(file_path, 0)
        blur = cv2.GaussianBlur(img,(3,3),0)
        th_img = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
        cv2.imshow("Original", img)
        cv2.imshow("Blur", th_img)
        img_resize = cv2.resize(th_img, (28, 28), interpolation=cv2.INTER_AREA)
        img_resize = np.array(img_resize).reshape(1, 28, 28, 1)
    else:
        print("USAGE: digit_preprocess.py <file>")
    if k == ord('p'):
        y_hat = model.predict(img_resize)
        if y_hat > 0.5:
            print("Even")
        else:
            print("Odd")
        break
    if k == ord('h'):
        print("Press p to see the prediction")
