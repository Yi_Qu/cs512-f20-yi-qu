cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)

def MY_CONV(unsigned char[:,:] img, double[:,:] kmtr, double[:,:] ret, int ksize):
    cdef cols = img.shape[0]
    cdef rows = img.shape[1]
    cdef x_max = rows-ksize
    cdef y_max = cols-ksize
    # mem_view = np.zeros((cols-ksize, rows-ksize), dtype=np.double)
    # cdef double[:,:] ret = mem_view
    
    for x from 0<=x<x_max:
        for y from 0<=y<y_max:
            value = 0
            # total = 0
            for i from 0<=i<ksize:
                for j from 0<=j<ksize:
                    value += img[y+j,x+i] * kmtr[j,i]
            ret[y,x] = value
    # return ret
