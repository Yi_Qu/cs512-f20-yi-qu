import numpy as np
import cv2
import sys
import os
import math
import time
import MY_CONV


iter = 0
cwd = os.getcwd()
img = [0]
file_path = '../data/out.jpg'

def on_change(self):
    pass

def trackbar_rotate(img):
    cv2.namedWindow('Rotation')
    low_pos = 1
    high_pos = 359
    cv2.createTrackbar('Angle', 'Rotation', low_pos, high_pos, on_change)
    while(True):
        bar_pos = cv2.getTrackbarPos('Angle', 'Rotation')
        bar_pos = bar_pos + (bar_pos + 1) % 2
        rot_m = cv2.getRotationMatrix2D((img.shape[1]/2, img.shape[0]/2), bar_pos, 1)
        rot_img = cv2.warpAffine(img, rot_m, (img.shape[1], img.shape[0]))
        cv2.imshow('Rotation', rot_img)
        kk = cv2.waitKeyEx(100) & 0xEFFFFF
        if kk == ord('r'):
            cv2.destroyWindow('Rotation')
            break

def trackbar_plot(img):
    empty_img = np.zeros((img.shape[0], img.shape[1], 3), float)
    K = 50
    cv2.namedWindow('Plot')
    low_pos = 10
    high_pos = img.shape[-1] // 10
    if high_pos < 10:
        high_pos = img.shape[-1] % 31
    cv2.createTrackbar('Grad', 'Plot', low_pos, high_pos, on_change)
    dx, dy = np.gradient(img)
    mag = cv2.magnitude(dx, dy)
    mag_norm = cv2.normalize(mag, None, 0, K, cv2.NORM_MINMAX)
    angs = np.arctan(dy/(dx+0.0000001))
    while(True):
        bar_pos = cv2.getTrackbarPos('Grad', 'Plot')
        bar_pos = bar_pos + (bar_pos + 1) % 2
        rng_x = np.arange(0, dx.shape[0], bar_pos)
        rng_y = np.arange(0, dy.shape[1], bar_pos)
        empty_img = np.zeros((img.shape[0], img.shape[1], 3), float)
        for i in rng_x:
            for j in rng_y:
                p0 = (j, i)
                p1x = (i+mag_norm[i][j]*np.cos(angs[i][j])).astype(int)
                p1y = (j+mag_norm[i][j]*np.sin(angs[i][j])).astype(int)
                p1 = (p1y, p1x)
                cv2.arrowedLine(empty_img, p0, p1, (0,255,0), 1)
        cv2.imshow('Plot', empty_img)
        kk = cv2.waitKeyEx(100) & 0xEFFFFF
        if kk == ord('p'):
            cv2.destroyWindow('Plot')
            break

def trackbar_smooth(img):
    cv2.namedWindow('Smooth')
    low_pos = 3
    high_pos = 27
    cv2.createTrackbar('Blur', 'Smooth', low_pos, high_pos, on_change)
    while(True):
        bar_pos = cv2.getTrackbarPos('Blur', 'Smooth')
        bar_pos = bar_pos + (bar_pos + 1) % 2
        smooth_img = cv2.medianBlur(img, bar_pos)
        cv2.imshow('Smooth', smooth_img)
        kk = cv2.waitKeyEx(100) & 0xEFFFFF
        if kk == ord('s'):
            cv2.destroyWindow('Smooth')
            break

def trackbar_mysmooth(img):
    cv2.namedWindow('My Smooth')
    prev_bar_pos = 0
    kmtr = 0.0
    ret = 0.0
    sigma = 1
    low_pos = 3
    high_pos = 27
    cv2.createTrackbar('Blur', 'My Smooth', low_pos, high_pos, on_change)
    while(True):
        bar_pos = cv2.getTrackbarPos('Blur', 'My Smooth')
        bar_pos = bar_pos + (bar_pos + 1) % 2
        if prev_bar_pos != bar_pos:
            '''
            kmtr = np.fromfunction(lambda x, y: (1/(2*math.pi*sigma**2)) * math.e **
                             ((-1*((x-(bar_pos-1)/2)**2+(y-(bar_pos-1)/2)**2))/(2*sigma**2)),
                             (bar_pos, bar_pos))
            '''
            kmtr = 1 / bar_pos**2*np.ones((bar_pos, bar_pos), dtype=np.double)
            ret = np.zeros((img.shape[0]-bar_pos, img.shape[1]-bar_pos), dtype=np.double)
            MY_CONV.MY_CONV(img, kmtr, ret, bar_pos)
            smooth_img = np.array(ret).astype(np.uint8)
            cv2.imshow('My Smooth', smooth_img)
        prev_bar_pos = bar_pos
        kk = cv2.waitKeyEx(100) & 0xEFFFFF
        if kk == ord('S') or kk == ord('u'):
            cv2.destroyWindow('My Smooth')
            break

while(True):
    k = cv2.waitKeyEx(100) & 0xEFFFFF
    if len(sys.argv) >= 2:
        file_path = sys.argv[1]
        img = cv2.imread(file_path)
        cv2.imshow('Image', img)
    else:
        cap = cv2.VideoCapture(0)
        rect, img = cap.read()
        cv2.imshow('Camera', img)
        if k == ord(' '):
            cv2.imwrite('../data/out_camera.jpg', img)
            print("Image captured from camera")
            cv2.imshow('Image', img)
            cap.release()
            cv2.destroyWindow('Camera')
            continue
    if k == ord('q'):
        cv2.destroyAllWindows()
        break
    elif k == ord('i'):
        print("Reload")
        if len(file_path) >= 2:
            img = cv2.imread(file_path)
        else:
            img = cv2.imread('../data/out.jpg')
        cv2.imshow('Reload img', img)
        continue
    elif k == ord('w'):
        print("Save image to out.jpg")
        cv2.imwrite('../data/out.jpg', img)
        file_path = '../data/out.jpg'
        save += 1
        continue
    elif k == ord('g'):
        print("Convert to grayscale")
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        cv2.imshow('Gray Scale', img)
        continue
    elif k == ord('c'):
        print("Show single channel")
        if len(img) == 1:
            img = cv2.imread(file_path)
        cv2.imshow('Single Channel', img[:, :, iter%3])
        iter += 1
        continue
    elif k == ord('s'):
        print("Smooth")
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        trackbar_smooth(img)
        continue
    elif k == ord('S') or k == ord('u'): # my smooth
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        trackbar_mysmooth(img)
        continue
    elif k == ord('D') or k == ord('e'):
        if len(img) == 1:
            img = cv2.imread(file_path)
        down_img = cv2.resize(img, (0,0), fx=0.5, fy = 0.5,
                              interpolation=cv2.INTER_NEAREST)
        cv2.imshow('Down sampling with smooth', down_img)
        continue
    elif k == ord('d'):
        if len(img) == 1:
            img = cv2.imread(file_path)
        down_img = np.delete(img, np.s_[::2], 1)
        down_img = np.delete(down_img, np.s_[::2], 0)
        cv2.imshow('Down sampling', down_img)
    elif k == ord('x'):
        if len(img) == 1:
            img = cv2.imread('../data/out.jpg', 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
        cv2.normalize(sobelx, None, 0, 255, cv2.NORM_MINMAX)
        cv2.imshow('Normalize x', sobelx)
        continue
    elif k == ord('y'):
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)
        cv2.normalize(sobely, None, 0, 255, cv2.NORM_MINMAX)
        cv2.imshow('Normalize y', sobely)
        continue
    elif k == ord('m'):
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
        sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)
        mag = cv2.magnitude(sobelx, sobely)
        cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        cv2.imshow('Magnitude', mag)
        continue
    elif k == ord('p'):
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        trackbar_plot(img)
        continue
    elif k == ord('r'):
        if len(img) == 1:
            img = cv2.imread(file_path, 0)
        else:
            img = cv2.imread(sys.argv[1], 0)
        trackbar_rotate(img)
        continue
    elif k == ord('G') or k == ord('o'):
        if len(img) == 1:
            img = cv2.imread(file_path)
        gray_img = np.zeros(img.shape, dtype=int)
        red_ch = np.array(img[:,:,2]*0.299)
        green_ch = np.array(img[:,:,1]*0.587)
        blue_ch = np.array(img[:,:,0]*0.114)
        gray_img = (red_ch + green_ch + blue_ch).astype(np.uint8)
        cv2.imshow('My Gray', gray_img)
        continue
    elif k == ord('h'):
        print('The program is meant to provide some basic operations of image\
                using openCV and python. You can pass the directory of a\
                target file as one argument. If no argument is passing,\
                the program will open the camera and take a picture when\
                you press space.')
        print('Keys and operations it supports:')
        print('i: reload original image')
        print('w: save current image to ../data/out.jpg')
        print('g: convert image to grayscale using openCV function')
        print('G or o: convert the current image to grayscale using my function')
        print('c: cycle through color channels of image')
        print('s: convert image to grayscale and smooth it using opcnCV function')
        print('S or u: convert image to grayscale and smooth it using my function')
        print('d: downsample the image by 2 without smoothing')
        print('D or e: downsample the image by 2 with smoothing')
        print('x: convert image to grayscale and perform convolution with x\
              derivative filter')
        print('y: convert image to grayscale and perform convolution with y\
              derivative filter')
        print('m: show the magnitude of gradient vectors normalized it to [0, 255]')
        print('p: convert image to grayscale and plot gradient vector every N\
              pixels with each vector having a length of 30 pixels')
        print('r: convert image to grayscale and rotate it')
        print('h: display command line arguments and the keys it supports')
