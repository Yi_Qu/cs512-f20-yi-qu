\documentclass{article}
\usepackage{amsmath, amssymb, enumitem, graphicxi, minipage}

\title{Implementation of FDA: Fourier Domain Adaptation for Semantic Segmentation\\
\large CS512 Project Report}
\author{Yi Qu A20393099}
\date{Nov 30 2020}

\begin{document}
\maketitle
\section{Abstract}
Domain Adaptation focuses on applying models on data which has different distribution from the data the models were trained on. Traditional methods rely on transfer learning, which trains the model with data from different distribution after the model trained on previous data. On one hand, sometimes training the model is costly. On the other hand, it still cannot be generalized to similar scenarios. Fourier Domain Adaptation (FDA) method provides a simple solution to this problem. In this article, the author will describe the mathematical background of FDA and the implementation method. After that, the author did several experiments and analyzed the results in details.
\section{Introduction}
Domain adaptation allows the model trained on data for some scenarios to be generalized to different scenarios. Sometimes, when the label for data is missing, the domain adaptation becomes necessary. Unsupervised Domain Adaptation (UDA) refers to the adaptation of model trained on annotated data to unannotated data. Semantic Segmentation on annotated data has been widly studied. Deep Neural Network structures with convolution show promissing results in this field. Fully covolutional networks (FCN) is one of the most generalized model, which outputs pixel-wise log likelihoods. Previous study showed that FCN 8s architecture works well on image segmentation problems. DeepLab used encoder-decoder architecture. On encoder phase, DeepLab model stacks convolutional layers with pooling of different dimensions. On decoder phase, DeepLab model upsamples the input layer in order to recover the original dimension of the image. This method also indicates good performance on segmentation fields.

State-of-the-art UDA methods requires adversarial training, which is computationally expensive. However, if the input of the original task is similar to which of the new task, adversarial training seems overkill. When input of both tasks has the same content but only differs in statistical data, for example expectation and standard deviation, simple procedures can be applied in order to align the distribution of the source and target data.

Maximum Mean Discrepancy (MMD), when applied to both source and target data, describes the difference between source and target data. Central Moment Discrepancy (CMD) also gives basic statistical information about source and target data. Minimizing those two values before doing transfer learning or domain adaptation showed some improvement. Deep learning methods are also widly used in domain adaptation. Cycada and DCAN simply align the features extracted from the model. Those models classified the features into two classes, one is domain sensative feature class, the other is domain invariant feature class. They both used adversarial learning which generates new images filling the gap between feature sensative and feature invariant classes. CLAN manually selected super pixels which are more representative than the others. Then, it force the input data to align those suepr pixels. However, this method required human experts. BDL used bidirectional learning, which reverse the input and output.

Those methods are complex in terms of both computation and implementation. Fourier transform preserves the low-level statistical information of both set of images in the amplitude part and stores the high-level statistical information in the angle part. Inspired by this feature, the author of the paper came up with an idea that by simply switching the amplitude parts between source and target images, the discrepancy of the source and target image can be reduced, thus, training on the transfered data can achieve domain adaptation.
\section{Method}
\subsection{FDA}
Fourier transform of images can be calculated in
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{fft.jpg}
  \caption{Fourier Transform}
  \label{Figure 1: Fourier Transform}
\end{figure}
where h and w are the height and width of image, m and n are the region to perform fourier transform. Tensorflow and numpy have very efficient algorithm to perform discrete fourier transform for images. In order to switch the low frequent region between source and target images, we need to apply a mask which takes center area of the image and set it to 1, the othre area set to 0 insread. The formula of mask is shown in this picture.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{mask.jpg}
  \caption{The mask}
  \label{Figure 2: The mask}
\end{figure}

By applying mask to source image and inverse mask to target image, we can use the formula shown in Figure 3 to calculate transformation of images.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{fda_formula.jpg}
  \caption{Formula to calculate FDA}
  \label{Figure 3: Formula to calculate single band FDA}
\end{figure}

Knowing that we introduce an additional parameter $\beta$, which indicates the area of region to swap frequency, the different choices of $\beta$ can affect the performance of this model. When $\beta = 0$, the source image does not transform to the target image. When $\beta = 1$, the source image is masked out so that only target image is fed in the model. Empirical study showed that $\beta leq 0.15$ has better performance. We conducted the experiments for $\beta = 0.01, 0.05, 0.09$. Figure 4 shows how $\beta$ will affect the transformation of images. The larger the $\beta$ is, the more information it gets from target image.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{beta.jpg}
  \caption{Effect of transformation size}
  \label{Figure 4: Effect of transformation size}
\end{figure}

Followed by the hint of the author, I set the $\beta = 0.09$ and computed the fourier transform of image. Figure 5 shows the results. The upper image is after the transformation, the lower image is the original. Figure 6 shows the segmentation label.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{ft.png}
  \caption{Result of FFT}
  \label{Figure 5: FFT result}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{gt.png}
  \caption{The label of target image}
  \label{Figure 6: The label of the target image}
\end{figure}


\subsection{Model}
Applying FDA on traditional segmentation DNN models, we need to adjust the loss function. Basic segmentation models use cross entropy loss. However, we find that adding the regularization terms on the traditional cross entropy loss can lead to a better performance. Figure 7 shows the regularization of single band FDA, where $\lambda_{ent} = 0.005$, $L_{ent} = (L_{target}^2 + 0.000001)^2$, $L_{target}$ is the cross entropy loss of target image.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{sbfda.jpg}
  \caption{The loss function of single band FDA}
  \label{Figure 7: Loss function of single band FDA}
\end{figure}

Knowing that we have multiple choice of $\beta$, the author conduct the experiment on 3 different values of $\beta$ and take the one with largest loss values to form a multi-band FDA.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{mbfda.jpg}
  \caption{The loss value of multi-band FDA}
  \label{Figure 8: The loss value of multi-band FDA}
\end{figure}

The authors of this paper also apply self-supervised learning method to this problem. Self-supervised learning views the prediction with high confidence as the ground truth. Once this prediction is wrong, the model should cast high penalty on this prediction. Figure 9 shows the loss function that the author used for self-supervised learning.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{ssl.jpg}
  \caption{The loss function for self-supervised learning}
  \label{Figure 9: The loss function for self-supervised learning}
\end{figure}

The authors used two DNN models as the backbone. One is Fully convolutional networks with VGG16 as pretrained weights, the other is DeepLab V2 with ResNet101. I implement FCN8s model. The architecture of this model is shown in Figure 10.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{fcn.jpg}
  \caption{Fully convolutional networks architecture.}
  \label{Figure 10: FCN 8s architecture}
\end{figure}

\section{Data}
The authors of this paper used 2 datasets as source, which are GTA5 and SYNTHIA. GTA5 is a dataset captured in a video game, it has 24996 images with resolution 1914 X 1052. They are classified into 33 segmentations, however, the author used 19 classes. In my experiment, I only use 13 classes due to the memory limitation of the hardware. SYNTHIA is also a image dataset that captured by the dash camera in front of cars. It has 9400 images with resolution 1280 X 760. It has 16 classes, but the authors of the paper only use 13 classes. Figure 11 and 12 show the image and the labels of GTA5.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{gta.jpg}
  \caption{GTA5 Image}
  \label{Figure 11: GTA5 Image}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{gta_lab.jpg}
  \caption{GTA5 Labels}
  \label{Figure 12: GTA5 Labels}
\end{figure}

The target image dataset is CityScapes, which is a real-world image dataset captured from car camera on the road. The authors used 2975 images from training set and 500 images from validation set as validations. Each image has resolution 1024 X 512. Figure 13 and 14 show the image and the labels of CitiScapes.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{cs.jpg}
  \caption{CityScapes Image}
  \label{Figure 13: CityScapes Image}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{cs_lab.jpg}
  \caption{CityScapes Labels}
  \label{Figure 14: CityScapes Labels}
\end{figure}

I only trained the model on GTA5 and CityScapes dataset since I cannot download the data from SYNTHIA. Due to the hardward limitation of my machine, I set the resolution of each image be 256 X 416 and only classify the dataset into 13 classes. The authors view GTA5 as source image and CitiScapes as target image. However, I reversed the experiment. Instead, I view CitiScapes as source image, GTA5 as target image.

\section{Experiments and  Analysis}
The authors conducted the experiments on GTA5 to CityScapes task. The results is shown on Figure 15. The first section indicates that the mean IoU of single band FDA. The second section shows the results with multi-band FDA compared to single band when $\beta=0.09$. The third section shows the results of self-supervised learning with previous 1 training label as ground truth. The forth section shows the results of self-supervised learning with previous 2 training label as ground truth.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{gta2cs_iou.jpg}
  \caption{The results on GTA5 to CityScapes}
  \label{Figure 15: The results on GTA5 to CityScapes}
\end{figure}

From this results we know that FDA is equivalent to several other models which had been proven works for segmentation. However, FDA has marginal improvement compared to Cycada from the second section. As we train the model for more time, the self-supervised learning performs better. However, adversarial learning can outperform FDA self-supervised learning with the same level of complexity of training. The good point is FDA is fairly simple, compared with adversarial learning. But with self-supervised learning on multi-band FDA, the training procedure takes more time than pure adversarial learning.

On top of that, the authors did not show how bad when we transfer from GTA5 model to CityScapes directly without any modifications. I did the experiment reversely. Figure 16 to Figure 19 show the results of the model directly trained on CityScapes.
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{tar1.png}
  \caption{The results on CityScapes}
  \label{Figure 16: The results on CityScapes}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{tar2.png}
  \caption{The results on CityScapes}
  \label{Figure 17: The results on CityScapes}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{tar3.png}
  \caption{The results on CityScapes}
  \label{Figure 18: The results on CityScapes}
\end{figure}


The mIoU value is 30.1 after I train the model for 20 epochs. Without any change, when I apply the model on GTA5, I got the mIoU droped to 13.9. When I apply FDA with $\beta = 0.09$, then the mIoU increased to 22.8. Figure 20 to 27 demonstrate the difference. 

\begin{figure}[h!]
  \includegraphics[width=\linewidth]{1.png}
  \caption{Without FDA}
  \label{Figure 20: Without FDA}
\end{figure}
\begin{figure}[h!]
  \includegraphics[width=\linewidth]{1_inp.png}
  \caption{With FDA}
  \label{Figure 21: With FDA}
\end{figure}
\section{Discussion}
\end{document}
