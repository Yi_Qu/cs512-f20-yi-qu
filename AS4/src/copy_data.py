import numpy as np
import pandas as pd
import cv2
import sys
from scipy.linalg import svd
from scipy.linalg import null_space


wxs = []
wys = []
wzs = []
ixs = []
iys = []

line = []
f1 = open('../data/noise2_world.txt', 'r')
f2 = open('../data/noise2_image.txt', 'r')
line = f1.readline()
line = f2.readline()
max_points = int(line)

for i in range(max_points):
    line1 = f1.readline()
    line2 = f2.readline()
    wx, wy, wz = line1.split()
    ix, iy = line2.split()
    wxs.append(float(wx))
    wys.append(float(wy))
    wzs.append(float(wz))
    ixs.append(float(ix))
    iys.append(float(iy))

f1.close()
f2.close()

w = open('../data/noise2.txt','w')
w.write(str(len(wxs))+'\n')
for i in range(max_points):
    line_w = [str(wxs[i]), str(wys[i]), str(wzs[i]), str(ixs[i]), str(iys[i])]
    w.writelines(" ".join(line_w))
    w.write('\n')
w.close()


