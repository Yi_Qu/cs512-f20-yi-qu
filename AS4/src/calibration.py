import numpy as np
import pandas as pd
import cv2
import sys
from scipy.linalg import svd
from scipy.linalg import null_space

if len(sys.argv) < 2:
    print("USAGE: python3 calibration <cor_points>")
    exit(1)

wxs = []
wys = []
wzs = []
ixs = []
iys = []

line = []
f = open(sys.argv[1], 'r')
line = f.readline()
max_points = int(line)

for i in range(max_points):
    line = f.readline()
    wx, wy, wz, ix, iy = line.split()
    wxs.append(float(wx))
    wys.append(float(wy))
    wzs.append(float(wz))
    ixs.append(float(ix))
    iys.append(float(iy))

A = []
for i in range(max_points):
    m = np.zeros((2, 12), float)
    m[0][0] = wxs[i]
    m[0][1] = wys[i]
    m[0][2] = wzs[i]
    m[0][3] = 1.0
    m[0][8] = -1.0 * ixs[i] * wxs[i]
    m[0][9] = -1.0 * ixs[i] * wys[i]
    m[0][10] = -1.0 * ixs[i] * wzs[i]
    m[0][11] = -1.0 * ixs[i] * 1.0
    m[1][4] = wxs[i]
    m[1][5] = wys[i]
    m[1][6] = wzs[i]
    m[1][7] = 1.0
    m[1][8] = -1.0 * iys[i] * wxs[i]
    m[1][9] = -1.0 * iys[i] * wys[i]
    m[1][10] = -1.0 * iys[i] * wzs[i]
    m[1][11] = -1.0 * iys[i] * 1.0
    A.append(m)
A = np.array(A).reshape((2*max_points, 12))
b = np.transpose(np.expand_dims(np.array([0]*2*max_points), axis=0))

U, D, Vt = svd(A)
sol = Vt[-1]
a1 = sol[0:3]
a2 = sol[4:7]
a3 = sol[8:11]
b = [sol[3],sol[7],sol[11]]

rho = 1/np.linalg.norm(a3)
u0 = rho ** 2 * np.dot(a1, a3)
v0 = rho ** 2 * np.dot(a2, a3)
av = np.sqrt(rho ** 2 * np.dot(a2, a2) - v0**2)
s = np.power(rho, 4)/av * (np.dot(np.cross(a1, a3), np.cross(a2, a3)))
au = np.sqrt(rho ** 2 * np.dot(a1, a1) - s ** 2 - u0 ** 2)
K = [[au, s, u0], [0, av, v0], [0, 0, 1]]
T = np.sign(b[2]) * rho * np.dot(np.linalg.inv(K), b)
r3 = np.sign(b[2]) * rho * a3
r1 = rho ** 2/av * (np.cross(a2, a3))
r2 = np.cross(r3, r1)
R = np.array([r1, r2, r3])
print("solution = ")
print("K = ")
print(K)
print("T = ")
print(T)
print("R = ")
print(R)

RT = np.hstack((R, np.transpose(np.expand_dims(T, axis=0))))
M = np.dot(K, RT)
WPS = np.array([wxs, wys, wzs, [1]*len(wxs)]).reshape((4, len(wxs)))
IPS = np.dot(M, WPS)
IPx = np.array(IPS[0]/IPS[2])
IPy = np.array(IPS[1]/IPS[2])
msex = np.sum(np.power(np.array(IPx - ixs), 2))/len(IPx)
msey = np.sum(np.power(np.array(IPy - iys), 2))/len(IPy)

print("Mean Square Error of x axis = " + str(msex))
print("Mean Square Error of y axis = " + str(msey))
