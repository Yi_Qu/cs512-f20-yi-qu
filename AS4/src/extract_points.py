import numpy as np
import pandas as pd
import cv2
import math
import sys


if len(sys.argv) < 2:
    print("USAGE: python3 calibration.py <filename>")
    exit(1)
else:
    file_path = sys.argv[1]
    img = cv2.imread(file_path)


i = 0
ix = 0
iy = 0
ixs = []
iys = []

world = pd.read_csv('../data/world.csv')
wxs = world['ixs']
wys = world['iys']
wzs = world['izs']

f = open('../data/cor_points.txt', 'w')
f.write(str(len(wxs))+'\n')
f.close()

points = pd.DataFrame(data={'ixs':ixs, 'iys':iys})
points.to_csv('../data/points.csv')

def draw(event, x, y, flags, params):
    global ix, iy, i
    if event == 1:
        ix = x
        iy = y
        points = pd.read_csv('../data/points.csv')
        ixs = list(points['ixs'])
        iys = list(points['iys'])
        ixs.append(ix)
        iys.append(iy)
        points = pd.DataFrame(data={'ixs':ixs, 'iys':iys})
        points.to_csv('../data/points.csv')
        cv2.circle(img, (ix, iy), 3, (0, 255, 0), -1)
        f = open('../data/cor_points.txt', 'a')
        line = [str(wxs[i]), str(wys[i]), str(wzs[i]), str(ixs[i]), str(iys[i])]
        f.writelines(" ".join(line))
        f.write('\n')
        f.close()
        i+=1


while(True):
    key = cv2.waitKey(100)
    cv2.namedWindow("Window", cv2.WINDOW_NORMAL)
    cv2.imshow("Window", img)
    cv2.setMouseCallback("Window", draw)
    if key == ord('q'):
        break
